# README #

Create a div in the DOM containing your specified text and rotate about the axis you specify at the speed you specify using absolutely zero css.

* Version 0.0.1

### Params: ###
* param String - text to rotate
* param Number - time to complete one rotation (in milliseconds)
* param String - axis to rotate about

### Usage: ###
`text_rotate("My text to rotate", 1000, "x");`

### Contact: ###
* wleehunter@gmail.com